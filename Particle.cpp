/*
 * Particle.cpp
 *
 *  Created on: Dec. 24, 2019
 *      Author: lisapal
 */

#include "Particle.h"
#include <math.h>
#include <stdlib.h>

namespace lp {

Particle::Particle(): m_x(0),m_y(0) {

//	m_x = ((2.0*rand())/RAND_MAX)-1; // [-1,1]
//	m_y = ((2.0*rand())/RAND_MAX)-1;

//	m_xspeed = 0.01*(((2.0* rand())/RAND_MAX) -1); // [-1,1] . Multiply by 0.001 to have a double division instead of integer division that will likely give 0
//	m_yspeed = 0.01*(((2.0* rand())/RAND_MAX) -1);

	init();

}

void Particle::init(){
	m_x = 0;
	m_y = 0;
	m_direction = (2 * M_PI * rand()) / RAND_MAX;
	m_speed = (0.04 * rand()) / RAND_MAX;
	m_speed *= m_speed; // speed of the particles increases as you go from inner to outer edge

}

Particle::~Particle() {
	// TODO Auto-generated destructor stub
}


void Particle::update(int interval){
	/*
	m_x += m_xspeed;
	m_y += m_yspeed;

	if(m_x < -1.0 || m_x >= 1.0){ // particle space is box [-1,1]
		m_xspeed = -m_xspeed;	  // bounces
	}

	if(m_y < -1.0 || m_y >= 1.0){ // particle space is box [-1,1]
			m_yspeed = -m_yspeed;	  // bounces
		}
	*/

	m_direction += interval* 0.0003;
	double xspeed = m_speed * cos(m_direction);
	double yspeed = m_speed * sin(m_direction);
	// the amount that we move each particle by when we run the update method is proportional to the amount of that
	//that has passed since we last moved the particle, ensuring that it will move at similar speeds in slower systems.
	m_x +=xspeed * interval;
	m_y += yspeed * interval;

	if(m_x < -1 ||  m_x > 1 || m_y < -1 || m_y > 1){
		init();

	}

	if(rand() < RAND_MAX/100){ // will re-initialize a particle randomly every 100 runs
		init();
	}

}
} /* namespace lp */
