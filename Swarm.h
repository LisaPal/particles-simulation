/*
 * Swarm.h
 *
 *  Created on: Dec. 24, 2019
 *      Author: lisapal
 */

#ifndef SWARM_H_
#define SWARM_H_

#include "Particle.h"

namespace lp {

class Swarm {
public:
	const static int NPARTICLES = 5000;
private:
	Particle  *m_pParticles; // particles it points at cannot be changed
	int lastTime;

public:
	Swarm();
	virtual ~Swarm();
	void update(int elapsed);
	const Particle * const getParticles(){return m_pParticles;};// const Particle means that the Particle cannot be changed either with the pointer that is being returned
};

} /* namespace lp */

#endif /* SWARM_H_ */
