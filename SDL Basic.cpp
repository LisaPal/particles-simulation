//============================================================================
// Name        : SDL.cpp
// Author      : Lisa Pal
// Version     :
// Copyright   : Your copyright notice
// Description : Hello World in C++, Ansi-style
//============================================================================

#include <iostream>
#include "SDL.h"
#include <math.h>
#include "Screen.h"
#include <stdlib.h>
#include <time.h>
#include "Swarm.h"
#include "Particle.h"

using namespace std;
using namespace lp;

int main() {
	srand(time(NULL)); // seeds random number generator to produce a diff. sequence of random numbers every time
	Screen screen;
	//screen.init();
	if(screen.init() == false){
		cout << "Error initialising SDL." << endl;
	}

	Swarm swarm;

	// "Event loop" : loop that runs as long as game/GUI is running. Update depending on events as quick as possible to create a smooth animation.

	while(true){
		int elapsed = SDL_GetTicks(); // # ms since program started
		//screen.clear();
		swarm.update(elapsed);

		// Go smoothly from the min and max color values (0-255)
		unsigned char green = (1 + sin(elapsed * 0.001)) * 128; // [-1,1] . Unsigned char can store max 255, so this is security to not get > 255.
		// Changing the next at different rates to get different hues
		unsigned char red = (1 + sin(elapsed * 0.002)) * 128;
		unsigned char blue = (1 + sin(elapsed * 0.003)) * 128;
		const Particle * const pParticles = swarm.getParticles();
		for(int i=0; i<Swarm::NPARTICLES; i++){
			Particle particle = pParticles[i];

			// Particle space was [-1,1]. Must map it to the screen space
			int x = (particle.m_x + 1)* Screen::SCREEN_WIDTH/2;
			int y = particle.m_y * Screen::SCREEN_WIDTH/2 + Screen::SCREEN_HEIGHT/2;

			screen.setPixel(x,y, red, green, blue);
		}

		screen.boxBlur();

		// Draw the screen
		screen.update();


		// Check for messages/ events
		if(screen.processEvents() == false){
			break;
		}

	}

	//cout << "Max: " << max << endl;

	screen.close();
	return 0; // program ran ok
}
