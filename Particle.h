/*
 * Particle.h
 *
 *  Created on: Dec. 24, 2019
 *      Author: lisapal
 */

#ifndef PARTICLE_H_
#define PARTICLE_H_

namespace lp {

struct Particle {
	double m_x; // want to change the values gradually, so we dont use integer
	double m_y;

//	double m_xspeed;
//	double m_yspeed;
private:
	double m_speed;
	double m_direction;
private:
	void init();

public:
	Particle();
	virtual ~Particle();
	void update(int interval);
};

} /* namespace lp */

#endif /* PARTICLE_H_ */
