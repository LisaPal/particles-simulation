# Particles Simulation

A particles simulation using the SDL (Simple Directmedia Layer) library. The particles explode from a center, expanding with a rotating movement. Some smokey effects added.


![](particles_v6.mp4) <br/>



<!--<figure class="video_container">-->
<!--  <video controls="true" allowfullscreen="true" poster="particle_poster.png">-->
<!--    <source src="https://gitlab.com/LisaPal/particles-simulation/-/blob/master/particles_v6.mp4" type="video/mp4">-->
<!--  </video>-->
<!--</figure>-->

# Installation

Download SDL from  https://www.libsdl.org/ . 

# Files 

Particle.cpp : implements the behaviour of a particular particle, such a speed and trajectory. <br/>
Swarm.cpp: a swarm is composed of several particles. Makes sure to update the particles so they don’t accumulate in the screen. <br/>
SDL Basic.cpp : makes use of the SLD library for the GUI. Specifies how the particles will be rendered in the screen. <br/>
Screen.cpp : The **main** file.  Creates a separate window showing the simulation, which can be closed by the user. <br/>

# Authors

Lisa Pal 